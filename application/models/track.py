from application import db, app
from category import Category


class Track(db.Model):
    __tablename__ = 'tracks'

    id = db.Column(db.Integer, primary_key=True)
    artist = db.Column(db.String(100), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    category_name = db.Column(db.String(100), db.ForeignKey(Category.name), nullable=False)

    category = db.relationship('Category', backref=db.backref('tracks', lazy='dynamic'))
    votes = db.relationship('Vote', cascade='all,delete', backref='track')

    def __init__(self, artist, name, category):
        self.artist = artist
        self.name = name
        self.category = category

    @property
    def rating(self):
        result = 0
        for vote in self.votes:
            result += vote.vote
        return result

    def is_vote_allowed(self, uid):
        from vote import Vote

        vote_entity = Vote.query.filter(Vote.uid == uid, Vote.track_id == self.id).first()
        allowed_up = vote_entity is None or (vote_entity.vote != app.config['TRACK_BASE_RATING'] and vote_entity.vote != 1)
        allowed_down = vote_entity is None or (vote_entity.vote != app.config['TRACK_BASE_RATING'] and vote_entity.vote != -1)

        result = {
            'allowed_up': allowed_up,
            'allowed_down': allowed_down
        }

        return result, vote_entity

    def add_vote(self, uid, vote):
        from vote import Vote


        vote_allowed, vote_entity = self.is_vote_allowed(uid)

        if (not vote_allowed['allowed_up'] and vote > 0) or (not vote_allowed['allowed_down'] and vote < 0):
            return 0

        if self.rating + vote - (vote_entity.vote if vote_entity is not None else 0) <= 0:
            db.session.delete(self)
            result = 1
        else:
            if vote_entity is None:
                vote_entity = Vote(uid, self, vote)
                db.session.add(vote_entity)
            else:
                vote_entity.vote = vote
            result =  2

        db.session.commit()

        return result
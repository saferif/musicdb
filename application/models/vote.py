from application import db
from track import Track


class Vote(db.Model):
    __tablename__ = 'votes'

    uid = db.Column(db.Integer, primary_key=True, autoincrement=False)
    track_id = db.Column(db.Integer, db.ForeignKey(Track.id, ondelete='CASCADE'), primary_key=True)
    vote = db.Column(db.Integer, nullable=False)

    #track = db.relationship('Track', backref=db.backref('votes', cascade='all,delete', lazy='dynamic'))

    def __init__(self, uid, track, vote):
        self.uid = uid
        self.track = track
        self.vote = vote
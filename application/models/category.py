from application import db
from sqlalchemy.dialects.mysql import LONGBLOB


class Category(db.Model):
    __tablename__ = 'categories'

    name = db.Column(db.String(100), primary_key=True)
    description = db.Column(db.Text, nullable=True)
    image = db.Column(LONGBLOB, nullable=False)

    def __init__(self, name, description=None):
        self.name = name
        self.description = description
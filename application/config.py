class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_SESSION_TOKEN_KEY = '_csrf_token'
    CSRF_PARAM_NAME = '_csrf_param'
    SECRET_KEY = '\x85\xe2\xe2 \x8f\x1c\xca\x04\xd3\xc5\xce:\xdb=X\x01\x01\xc0\xb2\x8f\xb1\xde\x05\r\x0f\xa1P\x8e\xac3\x14\xbe'

    VK_APP_ID = '4412091'
    VK_SHARED_SECRET = '2IHrCXCiA6S2i1RJsoUB'

    TRACK_BASE_RATING = 50
    REQUIRE_USER_REDIRECT_TO = 'index'


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'mysql://user@localhost/foo'


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql://musicdb:music_great_pass@localhost/musicdb'
from flask import Flask, request
from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug.contrib.fixers import ProxyFix
from lib.csrf import CSRF
from lib.requireuser import RequireUser
from lib.filters.divide import divide_filter
import config

app = Flask(__name__)
app.config.from_object(config.DevelopmentConfig)
app.wsgi_app = ProxyFix(app.wsgi_app)

csrf = CSRF(app)
require_user = RequireUser(app)

app.jinja_env.globals['csrf_param'] = app.config['CSRF_PARAM_NAME']
app.jinja_env.globals['csrf_token'] = csrf.generate_csrf_token
app.jinja_env.globals['url'] = lambda: request.path + ('?' + request.query_string if request.query_string else '')
app.jinja_env.filters['divide'] = divide_filter

db = SQLAlchemy(app)

import models
import handlers
from misc.inituser import init_user

@app.before_request
def before_request():
    init_user()
$(document).ready(function() {
    function delete_cookie ( cookie_name )
    {
        var cookie_date = new Date ( );  // Текущая дата и время
        cookie_date.setTime ( cookie_date.getTime() - 1 );
        document.cookie = cookie_name + "=; expires=" + cookie_date.toGMTString();
    }

    function signin() {
        VK.Auth.login(function(response) {
            if (response.session) {
                var container = $('.navbar-right');
                VK.Api.call('users.get', {
                    fields: 'photo_50',
                    v: '5.21'
                }, function(r) {
                    container.empty();
                    r = r.response[0];
                    container.append($('<li></li>').append(
                        $('<img>').addClass('avatar').attr('src', r.photo_50)
                    ).append(
                        $('<span></span>').addClass('user-name').text(r.first_name + ' ' + r.last_name)
                    )).append($('<li></li>').append($('<a></a>').text('Sign out').attr('href', '#').addClass('signout-btn').click(signout)));
                });
            }
        }, 8);
    }

    function signout() {
        var container = $('.navbar-right');
        container.empty();
        container.append($('<li></li>').append($('<div></div>').addClass('MojoShare').append(
            $('<a></a>').addClass('VK').addClass('share').addClass('minimalism').attr('id', 'login-button').text('Sign in with VK').click(signin)
        )));
        VK.Auth.logout(function(){console.log('!!!');});
    }

    $('.signout-btn').click(signout);

    VK.init({
      apiId: 4412091
    });
    $('#login_button').click(function() {
        signin();
    });
});
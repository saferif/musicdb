$(document).ready(function() {
    function loadTracks() {
        $('#track-list').spin('modal');
        $.get('/api/tracks/' + category_name, function(data) {
            $('#track-list').empty();
            $.each(data.result, function() {
                var that = this;
                VK.Api.call('audio.search', {
                    q: this.artist + ' - ' + this.name,
                    sort: 2,
                    count: 1,
                    v: '5.21'
                }, function(r) {
                    if (r.response && r.response.items.length > 0) {
                        var item = r.response.items[0];
                        var li = $('<div></div>').addClass('list-group-item');
                        var h4 = $('<h4></h4>').addClass('list-group-item-heading').text(item.artist + ' — ' + item.title);
                        var p = $('<p></p>').addClass('list-group-item-text');
                        var list = $('#track-list');
                        var audio = $('<audio controls></audio>').addClass('col-lg-9');
                        var source = $('<source type="audio/mpeg"></source>').attr('src', item.url);
                        var rating = $('<span></span>').text(that.rating);
                        var thumbs_up_span = $('<span></span>').addClass('glyphicon').addClass('glyphicon-thumbs-up');
                        var thumbs_down_span = $('<span></span>').addClass('glyphicon').addClass('glyphicon-thumbs-down');
                        var thumbs_up_btn = $('<button></button>')
                            .attr('type', 'button').addClass('btn').addClass('btn-success').click(function() {
                                list.spin('modal');
                                $.get('/api/track/' + that.id + '/add_vote', {vote: 1, _csrf_param: api_token}, function(data) {
                                    if (data.result) {
                                        rating.text(data.rating);
                                    }
                                }).always(function() {
                                    list.spin('modal');
                                });
                            }).append(thumbs_up_span);
                        var thumbs_down_btn = $('<button></button>')
                            .attr('type', 'button').addClass('btn').addClass('btn-danger').click(function() {
                                list.spin('modal');
                                $.get('/api/track/' + that.id + '/add_vote', {vote: -1, _csrf_param: api_token}, function(data) {
                                    if (data.result) {
                                        if (data.rating <= 0) {
                                            $(thumbs_down_btn.parent()).parent().remove()
                                        } else {
                                            rating.text(data.rating);
                                        }
                                    }
                                }).always(function() {
                                    list.spin('modal');
                                });
                            }).append(thumbs_down_span);
                        p.append(audio.append(source));
                        var rating_container = $('<div></div>').addClass('col-lg-3').addClass('rating-container').append(thumbs_up_btn).append(rating).append(thumbs_down_btn);
                        p.append(rating_container).append($('<div></div>').addClass('clearfix'));
                        li.append(h4).append(p);
                        list.append(li);
                    }
                });
            });
        }).always(function() {
            $('#track-list').spin('modal');
        });
    }

    $('#addTrackDialog .btn-success').click(function() {
        $this = $(this);
        var artist = $('#add-track-artist').val();
        var name = $('#add-track-name').val();

        if (artist == '' || name == '') return;

        $this.spin('modal');
        $.post('/api/tracks/' + category_name, {
            artist: artist,
            name: name,
            _csrf_param: api_token

        }, function(data) {
            if (data.result) {
                loadTracks();
                $('#addTrackDialog').modal('hide');
                $('#add-track-form')[0].reset();
            }
        }).always(function() {
                $this.spin('modal');
            });
    });

    VK.init({
      apiId: 4412091
    });
    loadTracks();
});
import datetime
from flask import abort, Response
from application import app
from application.models.category import Category


@app.route('/api/category_image/<name>')
def category_image(name):
    category = Category.query.filter(Category.name == name).first()
    if not category:
        abort(404)
    response = Response(category.image, mimetype='image/png')
    response.cache_control.max_age = 600
    response.headers.add('Last-Modified', datetime.datetime.now())
    return response
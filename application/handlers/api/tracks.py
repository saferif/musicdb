from application.models.track import Track
from application.models.category import Category
from application.models.vote import Vote
from application import app, csrf, db, require_user
from flask import request, jsonify, abort, g


@app.route('/api/tracks/<category_name>', methods=['GET', 'POST'])
@csrf.protect(methods=['POST'], remove_token=False)
@require_user.require
def tracks(category_name):
    if request.method == 'GET':
        tracks = Track.query.filter(Track.category_name == category_name).all()
        track_list = list()
        user = g.user

        for track in tracks:
            track_list.append({
                'id': track.id,
                'artist': track.artist,
                'name': track.name,
                'rating': track.rating,
                'vote_allowed': track.is_vote_allowed(user['id'])[0]
            })

        return jsonify(result=sorted(track_list, key=lambda x: x['id']))

    category = Category.query.filter(Category.name == category_name).first()

    if category is None:
        abort(404)

    track_artist = request.form['artist']
    track_name = request.form['name']

    if track_artist == '' or track_name == '':
        return jsonify(result=False)

    track = Track.query.filter(Track.name == track_name, Track.artist == track_artist, Track.category_name == category_name).first()

    if track is not None:
        return jsonify(result=False)

    track = Track(track_artist, track_name, category)
    user = g.user

    vote = Vote(user['id'], track, app.config['TRACK_BASE_RATING'])
    db.session.add(track)
    db.session.add(vote)
    db.session.commit()

    return jsonify(result=True)
from application.models.track import Track
from application import app, csrf, require_user
from flask import request, jsonify, abort, g


@app.route('/api/track/<int:id>/add_vote')
@csrf.protect(remove_token=False)
@require_user.require
def add_vote(id):
    try:
        vote = int(request.args['vote'])
    except ValueError:
        abort(400)

    track = Track.query.filter(Track.id == id).first()

    if track is None:
        abort(404)

    user = g.user

    result = track.add_vote(user['id'], vote)

    if result > 0:
        result = {
            'result': True,
            'rating': track.rating if result > 1 else -1
        }
    else:
        result = {
            'result': False
        }

    return jsonify(**result)
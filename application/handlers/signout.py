from flask import session, make_response, redirect, url_for
from application import app, csrf, require_user


@app.route('/signout')
@csrf.protect(remove_token=False)
@require_user.require(True)
def signout():
    session.pop('user')
    response = make_response(redirect(url_for('index')))
    response.delete_cookie('vk_app_' + app.config['VK_APP_ID'])
    return response
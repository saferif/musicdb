from flask import render_template, abort
from application import app, require_user
from application.models.category import Category


@app.route('/tracks/<category_name>')
@require_user.require(True)
def tracks_html(category_name):
    category = Category.query.filter(Category.name == category_name).first()

    if category is None:
        abort(404)
    return render_template('tracks.html', category=category)
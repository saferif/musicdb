import urllib
import urllib2


class RequestWithMethod(urllib2.Request):
    def __init__(self, *args, **kwargs):
        self._method = kwargs.pop('method', None)
        urllib2.Request.__init__(self, *args, **kwargs)

    def get_method(self):
        return self._method if self._method else super(RequestWithMethod, self).get_method()


def fetch_url(url, params={}, method='GET', headers={}):
    params = urllib.urlencode(params)
    if method in ['POST']:
        headers['Content-type'] = 'application/x-www-form-urlencoded'
        headers['Content-length'] = len(params)
        r = RequestWithMethod(url, params, headers, method=method)
    else:
        r = RequestWithMethod('?'.join((url, params)), headers=headers, method=method)
    try:
        f = urllib2.urlopen(r)
    except urllib2.URLError as e:
        if isinstance(e, urllib2.HTTPError):
            return e.read(), e.code
        else:
            return None, None
    return f.read(), f.code
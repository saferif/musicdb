def divide_filter(iterable, count):
    batch = list()
    for elem in iterable:
        batch.append(elem)
        if len(batch) == count:
            yield batch
            del batch[:]
    if len(batch) > 0:
        yield batch
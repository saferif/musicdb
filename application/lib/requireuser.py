from functools import wraps
from flask import g, redirect, url_for, abort, flash


class RequireUser(object):
    def __init__(self, app):
        self.redirect_to = app.config['REQUIRE_USER_REDIRECT_TO']

    def require(self, redirect_enabled):
        if isinstance(redirect_enabled, bool):
            to_redirect = redirect_enabled
            func = None
        else:
            to_redirect = False
            func = redirect_enabled

        def decor(fn):
            @wraps(fn)
            def wrapped(*args, **kwargs):
                if 'user' not in g:
                    if to_redirect:
                        flash('You should be logged in to view this page')
                        return redirect(url_for(self.redirect_to))
                    else:
                        abort(403)
                else:
                    return fn(*args, **kwargs)
            return wrapped

        if func is not None:
            return decor(func)
        else:
            return decor
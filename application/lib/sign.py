import functools
import math


sign = functools.partial(math.copysign, 1)
import string, random
from flask import request, session, abort
from functools import wraps


class CSRF(object):
    def __init__(self, app):
        self.SESSION_TOKEN_KEY = app.config['CSRF_SESSION_TOKEN_KEY']
        self.PARAM_NAME = app.config['CSRF_PARAM_NAME']

    def protect(self, methods=None, csrf_param_name=None, csrf_session_key=None, remove_token=True):
        def decor(fn):
            @wraps(fn)
            def wrapper(*args, **kwargs):
                CSRF_SESSION_TOKEN_KEY = csrf_session_key or self.SESSION_TOKEN_KEY
                CSRF_PARAM_NAME = csrf_param_name or self.PARAM_NAME

                if (methods is None) or (request.method in methods):
                    if request.method in ['POST']:
                        token = request.form.get(CSRF_PARAM_NAME)
                    else:
                        token = request.args.get(CSRF_PARAM_NAME)
                    s_token = None
                    try:
                        s_token = session.pop(CSRF_SESSION_TOKEN_KEY)
                    except KeyError:
                        abort(403)
                    if (not token) or (token != s_token):
                        session[CSRF_SESSION_TOKEN_KEY] = s_token
                        abort(403)
                    if not remove_token:
                        session[CSRF_SESSION_TOKEN_KEY] = s_token
                return fn(*args, **kwargs)
            return wrapper
        return decor

    def get_random_string(self, size=32, chars=string.ascii_letters + string.digits):
        return ''.join(random.choice(chars) for _ in xrange(size))

    def generate_csrf_token(self):
        if self.SESSION_TOKEN_KEY not in session:
            session[self.SESSION_TOKEN_KEY] = self.get_random_string()
        return session[self.SESSION_TOKEN_KEY]
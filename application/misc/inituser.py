import json
from flask import session, g
from ..lib.urlfetch import fetch_url
from application.misc.vk import authOpenAPIMember


def parse_user_session():
    user = session.get('user')
    if user is None:
        return None

    params = user.split('|')
    if len(params) != 5:
        return None

    return {
        'id': int(params[0]),
        'first_name': params[1],
        'last_name': params[2],
        'photo': params[3],
        'permissions': int(params[4])
    }

def get_user_from_vk(sid):
    content, status = fetch_url('https://api.vk.com/method/users.get', {
        'fields': 'photo_50',
        'access_token': sid,
        'v': '5.21'
    })
    response = json.loads(content)
    response = response['response'][0]
    result = [str(response['id']), response['first_name'], response['last_name'], response['photo_50']]

    content, status = fetch_url('https://api.vk.com/method/account.getAppPermissions', {
        'access_token': sid,
        'v': '5.21'
    })
    response = json.loads(content)
    response = response['response']
    result.append(str(response))

    session['user'] = '|'.join(result)
    return parse_user_session()


def init_user():
    user = authOpenAPIMember()
    if user:
        s_user = parse_user_session()
        if s_user is None or s_user['id'] != user['id']:
            s_user = get_user_from_vk(user['sid'])
        user.update(s_user)
        if user['permissions'] & 8 == 8:
            g.user = user
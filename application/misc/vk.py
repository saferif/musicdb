import hashlib, time
from flask import request
from application import app


def authOpenAPIMember():
    session = dict()
    member = False
    valid_keys = ['expire', 'mid', 'secret', 'sid', 'sig']
    app_cookie = request.cookies.get('vk_app_' + app.config['VK_APP_ID'])
    if app_cookie:
        session_data = app_cookie.split('&')
        for pair in session_data:
            key, value = pair.split('=')
            if (not key) or (not value) or (not key in valid_keys):
                continue
            session[key] = value
        for key in valid_keys:
            if not key in session:
                return member
        sign = ''
        for key, value in sorted(session.items()):
            if key != 'sig':
                sign += '='.join((key, value))
        sign += app.config['VK_SHARED_SECRET']
        sign = hashlib.md5(sign).hexdigest()
        if (session['sig'] == sign) and (session['expire'] > int(time.time())):
            member = {
                'id': session['mid'],
                'secret': session['secret'],
                'sid': session['sid']
            }
    return member